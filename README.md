Desafio Spotippos
---------

Desafio proposto pelo grupo Zap/Vivareal sobre a lenda de Spotippos, o tipo escolhido foi o de back-end.

Ambiente de desenvolvimento
---------

O ambiente de desenvolvimento local utiliza o `docker-compose` e devera ser utilizado da seguinte maneira

    $ docker-compose build
    $ docker-compose up

Utilize `docker-compose up -d` se desejar rodar a aplicacao em background.

Testes Unitarios
---------

Os testes podem ser executados da seguinte forma

    $ python tests.py

Tecnologias utilizadas
---------

Utilizei Flask e o Flask-Restful para desenvolver a API, tambem utilizei SQLite3 para o banco de dados e containers para o ambiente local e de staging/production. gitlab foi escolhido pois conta com um servico de CI/CD integrado. tambem utilizei o codecov.io para avaliar a cobertura de codigo.

Arquitetura e Deployment
---------

Optando pela facilidade, escolhi os servicos ECR e ECS da AWS para o deployment da aplicacao, dessa forma a configuracao fica mais clara e descomplicada, o padrao gitflow utilizado e o mais comum de feature branch entre development e master.

### Staging / Homologacao

O ambiente de staging deve contar com pelo menos 1 'running tasks' dependendo da carga de QA, no caso de um cluster multicloud (nao suportado pelo ECS) devera rodar 1 task/container em cada localidade/cloud para garantir disponibilidade.

### Production / Producao

O ambiente de producao sendo mais critico, deve ter pelo menos 3 running tasks/containers, tambem no caso de um cluster multicloud/hibrido, devera rodar um conjunto de 3 tasks/containers em cada localidade.


Requests & Responses
---------

### 1. Criacao de Propriedades

Request

    $ curl -iL -H "Content-Type: application/json" http://localhost:5000/properties \
        -d '{"x": 667,"y": 556,"title": "Imóvel código 1, com 5 quartos e 4 banheiros","price": 1250000,"description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.","beds": 4,"baths": 3,"squareMeters": 210}'

Response

    HTTP/1.0 201 CREATED
    Content-Type: application/json
    Connection: close
    Server: Werkzeug/0.14.1 Python/3.6.5
    Date: Sun, 17 Jun 2018 01:40:14 GMT

    {
      "property": {
        "baths": 3,
        "beds": 4,
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "id": 1,
        "price": 1250000,
        "squareMeters": 210,
        "title": "Imóvel código 1, com 5 quartos e 4 banheiros",
        "x": 667,
        "y": 556
      },
      "status": "success"
    }

### 1.1 Validacao de duplicidade em banco, baseado no campo `title`

Request

    $ curl -iL -H "Content-Type: application/json" http://localhost:5000/properties \
        -d '{"x": 667,"y": 556,"title": "Imóvel código 1, com 5 quartos e 4 banheiros","price": 1250000,"description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.","beds": 4,"baths": 3,"squareMeters": 210}'

Response

    HTTP/1.0 400 BAD REQUEST
    Content-Type: application/json
    Content-Length: 43
    Server: Werkzeug/0.14.1 Python/3.6.5
    Date: Sun, 17 Jun 2018 01:42:04 GMT

    {
      "message": "Property already exists"
    }

### 2. Informacoes de propriedade especifica

Request

    $ curl -iL -H "Content-Type: application/json" http://localhost:5000/properties/1

Response

    HTTP/1.0 200 OK
    Content-Type: application/json
    Connection: close
    Server: Werkzeug/0.14.1 Python/3.6.5
    Date: Sun, 17 Jun 2018 01:44:22 GMT

    {
      "baths": 3,
      "beds": 4,
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
      "id": 1,
      "price": 1250000,
      "provinces": [
        "Ruja"
      ],
      "squareMeters": 210,
      "title": "Imóvel código 1, com 5 quartos e 4 banheiros",
      "x": 667,
      "y": 556
    }

### 2.1 Informacoes de propriedade especifica inexistente

Request

    $ curl -iL -H "Content-Type: application/json" http://localhost:5000/properties/2

Response

    HTTP/1.0 404 NOT FOUND
    Content-Type: application/json
    Content-Length: 53
    Server: Werkzeug/0.14.1 Python/3.6.5
    Date: Sun, 17 Jun 2018 01:52:29 GMT

    {
      "property": "not found",
      "status": "failed"
    }


### 3. Busca de propriedades baseado em X & Y

Request

    $ curl -iL -H "Content-Type: application/json" \
        -X GET "http://localhost:5000/properties?ax=600&bx=680&ay=500&by=600"

Response

    HTTP/1.0 200 OK
    Content-Type: application/json
    Connection: close
    Server: Werkzeug/0.14.1 Python/3.6.5
    Date: Sun, 17 Jun 2018 01:50:54 GMT

    {
      "foundProperties": 1,
      "properties": [
        {
          "baths": 3,
          "beds": 4,
          "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
          "id": 1,
          "price": 1250000,
          "provinces": [
            "Ruja"
          ],
          "squareMeters": 210,
          "title": "Imóvel código 1, com 5 quartos e 4 banheiros",
          "x": 667,
          "y": 556
        }
      ]
    }


### 3.1 Busca de propriedade baseado em X & Y inexistente

Request

    $ curl -iL -H "Content-Type: application/json" \
        -X GET "http://localhost:5000/properties?ax=100&ay=700&bx=700&by=300"

Response

    HTTP/1.0 404 NOT FOUND
    Content-Type: application/json
    Content-Length: 48
    Server: Werkzeug/0.14.1 Python/3.6.5
    Date: Sun, 17 Jun 2018 01:46:08 GMT

    {
      "foundProperties": 0,
      "properties": []
    }
