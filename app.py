from flask import Blueprint
from flask_restful import Api
from resources.Properties import PropertyResource

api_bp = Blueprint('api', __name__)
api = Api(api_bp)

api.add_resource(PropertyResource,
    '/properties/<int:property_id>',
    '/properties')
