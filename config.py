import os

basedir = os.path.abspath(os.path.dirname(__file__))

DEBUG = True
JSON_AS_ASCII = False
SQLALCHEMY_ECHO = False
SQLALCHEMY_TRACK_MODIFICATIONS = True
SQLALCHEMY_DATABASE_URI = "sqlite:///spotippos.db"
