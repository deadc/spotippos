from flask import Flask
from marshmallow import Schema, ValidationError, fields, pre_load, validate
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

ma = Marshmallow()
db = SQLAlchemy()

def validate_beds(n):
    if not 1 <= n <= 5:
        raise ValidationError('must be between 1 and 5')

def validate_baths(n):
    if not 1 <= n <= 4:
        raise ValidationError('must be between 1 and 4')

def validate_squaremeters(n):
    if not 20 <= n <= 240:
        raise ValidationError('must be between 20 and 240')

def validate_x(n):
    if not 0 <= n <= 1400:
        raise ValidationError('must be between 0 and 1400')

def validate_y(n):
    if not 0 <= n <= 1000:
        raise ValidationError('must be between 0 and 1000')

class Property(db.Model):
    __tablename__ = 'properties'

    id              = db.Column(db.Integer, primary_key=True)

    title           = db.Column(db.String(250), nullable=False)
    description     = db.Column(db.String(250), nullable=False)

    price           = db.Column(db.Integer, nullable=False)
    x               = db.Column(db.Integer, nullable=False)
    y               = db.Column(db.Integer, nullable=False)
    beds            = db.Column(db.Integer, nullable=False)
    baths           = db.Column(db.Integer, nullable=False)
    squareMeters    = db.Column(db.Integer, nullable=False)

    def __init__(self, title, price, description, x, y, beds, baths, squareMeters):
        self.title          = title
        self.price          = price
        self.description    = description
        self.x              = x
        self.y              = y
        self.beds           = beds
        self.baths          = baths
        self.squareMeters   = squareMeters

class PropertySchema(ma.Schema):
    id           = fields.Integer(dump_only=True)

    price        = fields.Integer(required=True)
    x            = fields.Integer(required=True, validate=validate_x)
    y            = fields.Integer(required=True, validate=validate_y)
    beds         = fields.Integer(required=True, validate=validate_beds)
    baths        = fields.Integer(required=True, validate=validate_baths)
    squareMeters = fields.Integer(required=True, validate=validate_squaremeters)

    description  = fields.String(required=True)
    title        = fields.String(required=True)
