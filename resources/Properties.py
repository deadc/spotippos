from flask import jsonify, request, make_response
from flask_restful import Resource, reqparse
from model import db, Property, PropertySchema
from config import basedir
import json

property_schema = PropertySchema()
properties_schema = PropertySchema(many=True)

class PropertyResource(Resource):
    def find_province(self, px, py):
        provinces = []
        with open(basedir + '/provinces.json') as f:
            data = json.load(f)

        for province in data:
            bottom_right_x = data[province]['boundaries']['bottomRight']['x']
            bottom_right_y = data[province]['boundaries']['bottomRight']['y']

            upper_left_x   = data[province]['boundaries']['upperLeft']['x']
            upper_left_y   = data[province]['boundaries']['upperLeft']['y']

            if upper_left_x <= px <= bottom_right_x and bottom_right_y <= py <= upper_left_y:
                provinces.append(province)

        return provinces

    def get(self, property_id=False):
        if not property_id:
            params = request.args

            property = Property.query.filter(
                Property.x.between(params['ax'], params['bx']),
                Property.y.between(params['ay'], params['by'])
            ).all()

            result = properties_schema.dump(property).data

            for re in result:
                re['provinces'] = self.find_province(re['x'], re['y'])

            ret_code = 200 if len(property) > 0 else 404
            
            return make_response(jsonify({ "foundProperties": len(property), "properties": result }), ret_code)
        else:
            property = Property.query.filter_by(id=property_id).first()
            if property:
                result = property_schema.dump(property).data
                result['provinces'] = self.find_province(result['x'], result['y'])

                return make_response(jsonify(result), 200)
            else:
                return make_response(jsonify({ "status": "failed", "property": "not found" }), 404)

    def post(self):
        json_data = request.get_json(force=True)
        if not json_data:
            return make_response(jsonify({"message": "No input data provided"}), 400)

        #if json_data['lat'] and json_data['long']:
        if 'lat' in json_data and 'long' in json_data:
            json_data['x'] = json_data['lat']
            json_data['y'] = json_data['long']

        data, errors = property_schema.load(json_data)
        if errors:
            return make_response(jsonify(errors), 422)

        property = Property.query.filter_by(title=data['title']).first()
        if property:
            return make_response(jsonify({"message": "Property already exists"}), 400)

        property = Property(
            description  = data['description'],
            squareMeters = data['squareMeters'],
            baths        = data['baths'],
            price        = data['price'],
            title        = data['title'],
            beds         = data['beds'],
            x            = data['x'],
            y            = data['y'])

        db.session.add(property)
        db.session.commit()

        return make_response(jsonify({"status": "success", "property": property_schema.dump(property).data}), 201)
