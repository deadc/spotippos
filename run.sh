#!/bin/sh
# -- ++ --

if [ ! -d "migrations" ] ; then
  python migrate.py db init
fi

python migrate.py db migrate
python migrate.py db upgrade

python run.py
