import unittest
import json
from model import db
from run import create_app

DUMMY_PROPERTY = """
{
  "x": 222,
  "y": 444,
  "title": "Imóvel código 1, com 5 quartos e 4 banheiros",
  "price": 1250000,
  "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  "beds": 4,
  "baths": 3,
  "squareMeters": 210
}
"""

class TestConfig(object):
    SQLALCHEMY_DATABASE_URI = "sqlite:///"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    JSONIFY_PRETTYPRINT_REGULAR = False
    TESTING = True

class TestProperties(unittest.TestCase):
    def setUp(self):
        app = create_app(TestConfig())
        self.capp = app.test_client()
        app_ctx = app.app_context()
        app_ctx.push()
        db.create_all()

    def test_properties_get(self):
         response = self.capp.get('/properties/1', content_type='application/json')
         data = json.loads(response.get_data(as_text=True))
         self.assertEqual(data['id'], 1)
         self.assertEqual(data['x'], 222)
         self.assertEqual(data['y'], 444)
         self.assertEqual(data['title'], 'Imóvel código 1, com 5 quartos e 4 banheiros')
         self.assertEqual(data['price'], 1250000)
         self.assertEqual(data['description'], 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.')
         self.assertEqual(data['beds'], 4)
         self.assertEqual(data['baths'], 3)
         self.assertEqual(data['squareMeters'], 210)

    def test_properties_get_nothing(self):
         response = self.capp.get('/properties/2', content_type='application/json')
         data = json.loads(response.get_data(as_text=True))
         self.assertEqual(data['status'], 'failed')
         self.assertEqual(data['property'], 'not found')

    def test_properties_create_valid(self):
        content = json.loads(DUMMY_PROPERTY)
        response = self.capp.post('/properties', data=json.dumps(content), content_type='application/json')
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(data['status'], 'success')

        data = data['property']
        self.assertEqual(data['id'], 1)
        self.assertEqual(data['x'], 222)
        self.assertEqual(data['y'], 444)
        self.assertEqual(data['title'], 'Imóvel código 1, com 5 quartos e 4 banheiros')
        self.assertEqual(data['price'], 1250000)
        self.assertEqual(data['description'], 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.')
        self.assertEqual(data['beds'], 4)
        self.assertEqual(data['baths'], 3)
        self.assertEqual(data['squareMeters'], 210)

    def test_properties_create_invalid_beds(self):
        content = json.loads(DUMMY_PROPERTY)
        content['beds'] = 6
        response = self.capp.post('/properties', data=json.dumps(content), content_type='application/json')
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(data['beds'][0], 'must be between 1 and 5')

    def test_properties_create_invalid_baths(self):
        content = json.loads(DUMMY_PROPERTY)
        content['baths'] = 5
        response = self.capp.post('/properties', data=json.dumps(content), content_type='application/json')
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(data['baths'][0], 'must be between 1 and 4')

    def test_properties_create_invalid_squaremeters(self):
        content = json.loads(DUMMY_PROPERTY)
        content['squareMeters'] = 241
        response = self.capp.post('/properties', data=json.dumps(content), content_type='application/json')
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(data['squareMeters'][0], 'must be between 20 and 240')

    def test_properties_create_invalid_x(self):
        content = json.loads(DUMMY_PROPERTY)
        content['x'] = 1401
        response = self.capp.post('/properties', data=json.dumps(content), content_type='application/json')
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(data['x'][0], 'must be between 0 and 1400')

    def test_properties_create_invalid_y(self):
        content = json.loads(DUMMY_PROPERTY)
        content['y'] = 1001
        response = self.capp.post('/properties', data=json.dumps(content), content_type='application/json')
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(data['y'][0], 'must be between 0 and 1000')

    def test_properties_find(self):
         response = self.capp.get('/properties?ax=0&bx=250&ay=0&by=450', content_type='application/json')
         data = json.loads(response.get_data(as_text=True))
         self.assertEqual(data['foundProperties'], 1)

         data = data['properties'][0]
         self.assertEqual(data['id'], 1)
         self.assertEqual(data['x'], 222)
         self.assertEqual(data['y'], 444)
         self.assertEqual(data['title'], 'Imóvel código 1, com 5 quartos e 4 banheiros')
         self.assertEqual(data['price'], 1250000)
         self.assertEqual(data['description'], 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.')
         self.assertEqual(data['beds'], 4)
         self.assertEqual(data['baths'], 3)
         self.assertEqual(data['squareMeters'], 210)
         self.assertEqual(data['provinces'][0], 'Scavy')

    def test_properties_find_nothing(self):
         response = self.capp.get('/properties?ax=1400&bx=1400&ay=1000&by=1000', content_type='application/json')
         data = json.loads(response.get_data(as_text=True))
         self.assertEqual(data['foundProperties'], 0)
         self.assertEqual(len(data['properties']), 0)

if __name__ == '__main__':
    unittest.main()
